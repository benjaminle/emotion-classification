#!/bin/bash

# run using bash run.bash 
# please make sure you have miniconda installed
# this script will help you create an isolated virtual environment, 
# install all necessary packages,
# and run the emotion recognition model

source ~/opt/miniconda3/etc/profile.d/conda.sh # or where ever your conda.sh file is
conda create -n emo-rec python=3.8   
conda activate emo-rec
which python
which pip
which pip3
pip install scipy
pip install librosa
pip install numpy
pip install pyaudio
pip install speechrecognition
pip install wavio
pip install transformers
pip install torch
pip install python_speech_features

python3 testing.py
