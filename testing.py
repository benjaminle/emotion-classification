from classifiers import combined_classifier as cc
import time
import librosa
import numpy as np
import scipy.io.wavfile
import pyaudio
import wave
import struct

emotion_dict = {4: lambda : 'anger',
                6: lambda : 'disgust',
                5: lambda : 'fear',
                2: lambda : 'happy',
                3: lambda : 'sad',
                7: lambda : 'surprised',
                1: lambda : 'calm',
                0: lambda : 'neutral'}

enhancedBert = cc.EnhancedBertForEmotionalClassification()
enhancedBert.float()



chunk = 1024  # Record in chunks of 1024 samples
sample_format = pyaudio.paInt16  # 16 bits per sample
channels = 1
fs = 44100  # Record at 44100 samples per second
seconds = 5
filename = "output.wav"

p = pyaudio.PyAudio()  # Create an interface to PortAudio

print('Recording')

stream = p.open(format=sample_format,
                channels=channels,
                rate=fs,
                frames_per_buffer=chunk,
                input=True)

  # Initialize array to store frames

# Repeat 10 cycles
for i in range (0, 10):
    frames = np.array([], dtype=np.int16)
    for i in range(0, int(fs / chunk * seconds)): # predict every 5 seconds
        stream_data = stream.read(chunk, exception_on_overflow=False)
        data_np = np.frombuffer(stream_data, dtype=np.int16)
        frames = np.concatenate((frames, data_np / 32768))

    prediction = enhancedBert.predict(frames, fs)
    prediction = emotion_dict[prediction.item()]()
    print(prediction)

print('Finished recording')

# Stop and close the stream
stream.stop_stream()
stream.close()
# Terminate the PortAudio interface
p.terminate()









