import speech_recognition as sr
import wavio
import base64
from python_speech_features import mfcc
from transformers import BertModel, AdamW, \
    get_linear_schedule_with_warmup, BertTokenizer
import json
import torch.nn as nn
import numpy as np
import io
import torch
from classifiers import text_classifier as tc
from classifiers import head_classifier as hc
from classifiers import audio_classifier as ac

class EnhancedBertForEmotionalClassification(nn.Module):
    def get_activation(self, name):
        def hook(model, input, output):
            self.audio_hook[name] = output.detach()

        return hook

    def __init__(self):
        super(EnhancedBertForEmotionalClassification, self).__init__()
        """ set up audio net"""
        self.audionet = ac.ConvNetWithAdaptivePooling()
        self.audionet.load_state_dict(
            torch.load('./pretrained_weights/ac_all_data_checkpoint.pth', map_location=torch.device('cpu')))
        self.audionet.float()
        self.audio_hook = {}
        self.audionet.fc3.register_forward_hook(self.get_activation('fc3'))

        """ set up textnet """
        self.config = tc.load_configuration(
            json.load(open("./ctac-config-files/benchmark_config.json")),
            json.load(open("./ctac-config-files/datasets_config.json")),
            train=False
        )
        self.tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")
        self.textnet = BertModel.from_pretrained(
            "./pretrained_weights/2496"
        )
        self.textnet.cuda()
        self.r = sr.Recognizer()

        """ set up classification head"""
        self.classification_head = hc.ClassificationHead()
        self.classification_head.load_state_dict(
            torch.load('./pretrained_weights/classhead.pth'))
        self.classification_head.float()

    def combined_representation(self, data, sampling_rate, sampwidth=3):
        """ input: audio as IO.BytesIO object """
        """ output: concatenated tensor of text and audio representations"""

        input_wav = io.BytesIO()
        wavio.write(input_wav, data, sampling_rate, sampwidth=1)
        input_wav.seek(0)

        with sr.AudioFile(input_wav) as source:
            audio = self.r.record(source)
            self.r.adjust_for_ambient_noise(source)

        try:
            # for testing purposes, we're just using the default API key
            # to use another API key, use `r.recognize_google(audio, key="GOOGLE_SPEECH_RECOGNITION_API_KEY")`
            # instead of `r.recognize_google(audio)`
            X_test, X_mask = tc.preprocess_bert([self.r.recognize_google(audio)], self.tokenizer, self.config,
                                             return_attention_masks=True)
            outputs = self.textnet(X_test, token_type_ids=None, attention_mask=X_mask)
            text_representation = outputs[1]  # 768 features tensor
        except sr.UnknownValueError:
            print("Google Speech Recognition could not understand audio")
            text_representation = torch.zeros(768)
        except sr.RequestError as e:
            print("Could not request results from Google Speech Recognition service; {0}".format(e))
            text_representation = torch.zeros(768)

        mfcc_feat = [[mfcc(data, sampling_rate, numcep=30, nfilt=30)]]
        mfcc_feat = torch.tensor(mfcc_feat)
        self.audionet(mfcc_feat.float())

        combined_feats = torch.cat((text_representation.cpu(), self.audio_hook['fc3'].cpu()), 1).cpu().detach()

        return combined_feats

    def predict(self, data, sampling_rate):
        # data will be a numpy object, sampling_rate an int
        combined_feats = self.combined_representation(data, sampling_rate)
        outputs = self.classification_head(combined_feats.float())
        _, predicted = torch.max(outputs.data, 1)

        return predicted

