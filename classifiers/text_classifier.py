import torch
from transformers import BertForSequenceClassification, AdamW, \
    get_linear_schedule_with_warmup, BertTokenizer
from transformers import BertTokenizer
import os

# function for preprocessing sentences for bert training
def preprocess_bert(sentences, tokenizer, config, return_attention_masks=True):
    """
    perform the following steps:
    1. add [CLS] token to the start of every sentence
    2. add [SEP] token to the end of every sentence
    3. add [PAD] token to pad/truncate sentences into a pre-defined fixed length
    4. use the `tokenizer` to tokenize the sentences
    5. use the `tokenizer` to convert the sentences into indices
    6. convert indexed sentences to pytorch tensors
    remember to move the returned tensor to gpu (if necessary)
    :param sentences: a list of sentences that need to be preprocessed
    :param tokenizer: bert tokenizer to be used for the preprocessing
    :return: preprocessed list of sentences
    :return: preprocessed list of sentences, attention masks if `attention_masks` is set to `True`
    """

    # embedding_config = json.load(open("utils/preprocessing_config/embedding_config.json", "r"))
    max_length = config["dataset"]["max_sequence_length"]  # embedding_config["max_length"]
    attention_masks = []

    for i, sentence in enumerate(sentences):
        # modify in-place!
        sentences[i] = "[CLS] " + sentence
        sentences[i] += " [SEP]"

    tokenized_sentences = [tokenizer.tokenize(sentence) for sentence in sentences]
    indexed_tokens = [tokenizer.convert_tokens_to_ids(tokenized_sentence) for tokenized_sentence in tokenized_sentences]

    for tokens in indexed_tokens:
        tokens_length = len(tokens)
        if return_attention_masks:
            # when an array is multiplied by a negative number, the result is just `[]`
            attention_masks.append([1] * min(tokens_length, max_length) + [0] * (max_length - tokens_length))
        if tokens_length < max_length:
            tokens.extend([0] * (max_length - tokens_length))
        elif tokens_length > max_length:
            # reminder: use `del` to modify list in-place
            del tokens[max_length:]

    indexed_tokens = torch.tensor(indexed_tokens)

    return indexed_tokens.to("cuda"), torch.tensor(attention_masks).to("cuda") \
        if return_attention_masks else indexed_tokens.to("cuda")

def load_configuration(config, datasets_config, train=True):
    """
    Utility function to use before training.
    :param config: the training configuration file
    :param datasets_config: the datasets configuration file
    :param train: whether loading configuration for train or test
    :return: configuration as dictionary
    """

    # using a dictionary to facilitate retrieving variables
    combined_config = {}

    # add configuration for dataset
    dataset_name = config["dataset"]["name"]
    try:
        combined_config["dataset"] = {}
        combined_config["dataset"].update(config["dataset"])
        combined_config["dataset"].update(datasets_config[dataset_name])
        combined_config["dataset"]["name"] = dataset_name
    except KeyError:
        raise ValueError("The dataset \"" + dataset_name + "\" is not available")

    # add configuration for model
    combined_config["model"] = {}
    combined_config["model"].update(config["model"])
    combined_config["model"]["num_labels"] = config["dataset"]["num_classes"]

    # add configuration for visuals
    combined_config["visuals"] = {}
    combined_config["visuals"].update(config["visuals"])

    if train:
        combined_config.update(config["train"])
    else:
        combined_config["batch_size"] = config["batch_size"]
        combined_config["use_dataset"] = config["use_dataset"]

    # calculate number of batches in each =part of the dataset
    combined_config["batches_per_epoch"] = \
        combined_config["dataset"]["train_total_length"] // combined_config["batch_size"]
    # bad implementation (for benchmarking), fix later
    combined_config["batches_per_train"] = combined_config["batches_per_epoch"]
    combined_config["batches_per_validation"] = \
        combined_config["dataset"]["validation_total_length"] // combined_config["batch_size"]
    combined_config["batches_per_test"] = \
        combined_config["dataset"]["test_total_length"] // combined_config["batch_size"]

    # add configuration specific to training; print out configuration if necessary
    if train:
        combined_config["total_steps"] = combined_config["epochs"] * combined_config["dataset"]["train_total_length"]

        combined_config["batches_per_save"] = config["saving"]["batches_per_save"] \
            if config["saving"]["batches_per_save"] else combined_config["batches_per_epoch"]
        combined_config["save_folder"] = config["saving"]["save_folder"]

        if combined_config["verbose"] > 0:
            for key, value in combined_config.items():
                print(key + ":", value)

    return combined_config

def load_model(config, train=True):
    if config["model"]["type"] not in ["bert"]:
        raise ValueError("The model\"" + config["model"] + "\" is not available")

    if config["model"]["type"] == "bert":
        # `use_pretrained` --> use model trained and saved locally on the machine
        if not train or config["model"]["use_pretrained"]:
            model = BertForSequenceClassification.from_pretrained(
                config["model"]["pretrained_folder"]
            )
        else:
            # use pre-trained model from hugging face
            model = BertForSequenceClassification.from_pretrained(
                "bert-base-uncased",
                num_labels=config["model"]["num_labels"],
                output_attentions=False,
                output_hidden_states=False
            )
        model.cuda()
        tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")

        if train:
            optimizer = AdamW(
                model.parameters(),
                lr=config["learning_rate"],
                eps=1e-8
            )
            scheduler = get_linear_schedule_with_warmup(
                optimizer,
                num_warmup_steps=0,
                num_training_steps=config["total_steps"]
            )

            if config["model"]["use_pretrained"]:
                optimizer.load_state_dict(torch.load(
                    os.path.join(config["model"]["pretrained_folder"], "optimizer.pt"
                )))

            return model, optimizer, scheduler, tokenizer
        return model, tokenizer

