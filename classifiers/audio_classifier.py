import torch.nn as nn
import torch.nn.functional as F

# possible neural nets model : all conv so far

class MFCCNet(nn.Module):
    
#     MODEL 1
    """ Architecture: conv --> max --> conv --> max --> fc * 3"""
    def __init__(self):
        super(MFCCNet, self).__init__()
        self.conv1 = nn.Conv2d(1, 6, 3, stride=1, padding=(1,1))
        self.pool = nn.MaxPool2d(2, 2)  
        self.conv2 = nn.Conv2d(6, 16, (6, 4), stride = 1)
        self.fc1 = nn.Linear(16 * 70 * 6, 2000)
        self.fc2 = nn.Linear(2000, 750)
        self.fc3 = nn.Linear(750, 64)
        self.fc4 = nn.Linear(64, 8)

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = x.view(-1, 16 * 70 * 6)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = F.relu(self.fc3(x))
        x = self.fc4(x)
        return x

class MFCCNet2(nn.Module):
    
    """ Architecture: conv --> max --> conv --> max --> fc * 3"""
    def __init__(self):
        super(MFCCNet2, self).__init__()
        self.conv1 = nn.Conv2d(1, 6, (4,1), stride=(2,1))
        self.pool = nn.MaxPool2d(2, 2)  
        self.conv2 = nn.Conv2d(6, 16, (4, 3), stride = (2,2), padding = (1,1))
        self.fc1 = nn.Linear(16* 18 * 4, 512)
        self.fc2 = nn.Linear(512, 64)
        self.fc3 = nn.Linear(64, 8)

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = x.view(-1, 16 * 18 * 4)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

class ConvNetWithAdaptivePooling (nn.Module):
    def __init__(self):
        super(ConvNetWithAdaptivePooling, self).__init__()
        self.conv1 = nn.Conv2d(1, 6, 3, stride=1, padding=(1, 1))
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, (6, 4), stride=1)
        self.adaptivepool = nn.AdaptiveAvgPool2d((70, 6))
        self.fc1 = nn.Linear(16 * 70 * 6, 2000)
        self.fc2 = nn.Linear(2000, 750)
        self.fc3 = nn.Linear(750, 64)
        self.fc4 = nn.Linear(64, 8)


    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.adaptivepool(F.relu(self.conv2(x)))
        x = x.view(-1, 16 * 70 * 6)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = F.relu(self.fc3(x))
        x = self.fc4(x)
        return x

class ConvNetWithAdaptivePoolingV2(nn.Module):
    def __init__(self):
        super(ConvNetWithAdaptivePoolingV2, self).__init__()
        self.conv1 = nn.Conv2d(1, 6, 3, stride=1, padding=(1, 1))
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 10, (6, 4), stride=1)
        self.adaptivepool = nn.AdaptiveAvgPool2d((70, 6))
        self.fc1 = nn.Linear(10 * 70 * 6, 1500)
        self.fc2 = nn.Linear(1500, 750)
        self.fc3 = nn.Linear(750, 64)
        self.fc4 = nn.Linear(64, 8)

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.adaptivepool(F.relu(self.conv2(x)))
        x = x.view(-1, 10 * 70 * 6)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = F.relu(self.fc3(x))
        x = self.fc4(x)
        return x