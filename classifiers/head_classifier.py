import torch.nn.functional as F
import torch.optim as optim
import torch.nn as nn


class ClassificationHead(nn.Module):
    def __init__(self):
        super(ClassificationHead, self).__init__()
        self.fc1 = nn.Linear(832, 125)
        self.fc2 = nn.Linear(125, 64)
        self.fc3 = nn.Linear(64, 8)

    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x