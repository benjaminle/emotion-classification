const express = require('express')
var cors = require('cors')
var glob = require( 'glob' )
  , path = require( 'path' );

glob.sync( './views/*.htrml' ).forEach( function( file ) {
  require( path.resolve( file ) );
});


const app = express()
app.use(cors())
const port = 3000

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/views/tensor.html')
})

app.get('/ctac-model.js', (req, res) => {
    res.sendFile(__dirname + '/ctac-model.js')
})

app.get('/tf.min.js', (req, res) => {
    console.log(__dirname + '/tf.min.js')
    res.sendFile(__dirname + '/tf.min.js')
})

app.get('/model.json', (req, res) => {
    console.log(__dirname + '/models/audio_net_web/model.json')
    res.sendFile(__dirname + '/model.json')
})

app.get('/group1-shard1of1', (req, res) => {
  res.sendFile(__dirname + '/group1-shard1of1')
})
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})